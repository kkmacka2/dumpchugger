#D U M P C H U G G E R
# Kyle Mackay
import h5py
import os
import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib import gridspec

################## START OF PREDEFINED FUNCTIONS ###############################	
#recursively print name of all objects and sub-objects in h5 file
def printname(name):
	print name
#recursively print name of all attributes in h5 file
def print_attrs(name, obj):
    print name
    for key, val in obj.attrs.iteritems():
        print "    %s: %s" % (key, val)
#get keys to all the objects in h5 file
def keys(f):
    return [key for key in f.keys()]
def Numpy_ify_indices(start_i,end_i):
	#array[ind:ind] returns an empty array, need array[ind-1:ind] to get a slice
	#Similarly, array[0:-1] doesn't actually give last element. array[0:None] will give full array
	for i_ind in range(len(start_i)):
		if end_i[i_ind] == -1:
			end_i[i_ind] = None	
		if start_i[i_ind] == end_ind[i_ind]:
			start_i[i_ind] -= 1
		elif start_i[i_ind] == 0:
			start_i[i_ind] == None
	return start_i, end_i

#get the last plane of data in a 3D data structure
def GetDataSlice_ijk(hf,loc,start_ind,end_ind):
	if loc in hf:
		h5data = hf.get(loc).value
		#now slice the data
		start_ind,end_ind = Numpy_ify_indices(start_ind,end_ind)
		nz=h5data.shape[0]; ny=h5data.shape[1]; nx=h5data.shape[2];
		h5data = np.squeeze(h5data[start_ind[2]:end_ind[2],start_ind[1]:end_ind[1],start_ind[0]:end_ind[0]])
	else:
		print loc, ' not found in h5 file!'
	return h5data

def GetYZMeans(twoDdata):
	if len(twoDdata.shape) == 2:
		y_avg = np.mean(twoDdata, axis=0)
		z_avg = np.mean(twoDdata, axis=1)
	else:
		print 'Something went wrong. GetYZMeans() was called with non-2D data'
	return y_avg, z_avg

def GetXYZMeans(threeDdata):
	if len(threeDdata.shape) == 3:
		x_avg = np.mean(threeDdata, axis=(1,2))
		y_avg = np.mean(threeDdata, axis=(0,2))
		z_avg = np.mean(threeDdata, axis=(0,1))
	else:
		print 'Something went wrong. GetXYZMeans() was called with non-3D data'
	return x_avg, y_avg, z_avg

def GetSnapShotTime(hf,loc):
	dset = hf[loc]
	time = dset.attrs['simTime']
	return time[0]

def GetCoords(hf,loc,start_i,end_i):
	xdat = []; ydat = []; zdat = [];
	SDim = np.count_nonzero(np.subtract(end_i,start_i)) #count number of dimensions in slice
	indDiff = np.absolute(np.subtract(end_i,start_i)) #size of specified region
	indDiff[indDiff > 0] = 1 	#set to 1 if region size is nonzero in particular direction
	#create empty arrays that we will (or won't) fill
	#this function will always return x,y,z even if z is empty (in the case of a 2D sim)
	xvec = np.empty(shape=(0))
	yvec = np.empty(shape=(0))
	zvec = np.empty(shape=(0))
	xmat = np.empty(shape=(0))
	ymat = np.empty(shape=(0))
	zmat = np.empty(shape=(0))
	#get coordinates for specified region
	xdat = GetDataSlice_ijk(hf,loc+"X",start_i,end_i) 
	xmat = np.squeeze(xdat)
	if indDiff[0] != 0:
		ax = 0
		if SDim == 3:
			xvec = np.mean(xdat, axis=(0,1))
		elif SDim == 2:
			xvec = np.mean(xdat, axis=ax)
		else:
			xvec = xdat
	else:
		xvec = np.mean(xdat)
	ydat = GetDataSlice_ijk(hf,loc+"Y",start_i,end_i)
	ymat = np.squeeze(ydat)
	if indDiff[1] != 0:
		ax = indDiff[0] # if x is flat, y will be the first coordinate in the slice
		if SDim == 3:
			yvec = np.mean(ydat, axis=(0,2))
		elif SDim == 2:
			yvec = np.mean(ydat, axis=ax)
		else:
			yvec = ydat
	else:
		yvec = np.mean(ydat)
	if len(indDiff) == 3:
		containsZ = loc+"Z" in hf
		if containsZ:
			zdat = GetDataSlice_ijk(hf,loc+"Z",start_i,end_i)
			zmat = np.squeeze(zdat)
			if indDiff[2] != 0:
				ax = indDiff[0] + indDiff[1]
				if SDim == 3:
					zvec = np.mean(zdat, axis=(1,2))
				elif SDim == 2:
					zvec = np.mean(zdat, axis=ax)
				else:
					zvec = zdat
			else:
				zvec = np.mean(zdat)
		else:
			print "ERROR: Z-Coordinates not found in .h5 file!!! Could it be a 1D or 2D data set?"
	return xvec,yvec,zvec,xmat,ymat,zmat

def InitVarList(variableList):
	vlist = []
	for var in variableList:
		vlist.append([])
	return vlist

def SaveDatatoFile(dumpName,tc,vlist,varnames,udfvals=[],udfnames=[],xc=[],yc=[],zc=[]):
	savecommand = 'np.savez(dumpName'
	for i in range(len(varnames)):
		savecommand = savecommand+','+varnames[i]+'=vlist['+str(i)+']'
	if len(udfnames) > 0:
		if len(udfnames) != len(udfvals):
			print 'SaveDatatoFile ERROR: Number of UDF names not equal to number of UDFs !'
		for i in range(len(udfnames)):
			savecommand = savecommand+','+udfnames[i]+'=udfvals['+str(i)+']'	
	if np.size(xc) > 0:
		savecommand = savecommand+','+'x=xc'
	if np.size(yc) > 0:
		savecommand = savecommand+','+'y=yc'
	if np.size(zc) > 0:
		savecommand = savecommand+','+'z=zc'
	savecommand = savecommand + ')'
	print 'Saving state vars to compressed numpy .npz file '+dumpName+'.npz with the command:'	
	print '\t'+savecommand
	eval(savecommand)
	return

def PrintStatsToCSV(fNamePrefix,names,units,tc,mins,maxes,means):
	dirName = 'figures'
	tc = np.reshape(tc,(len(tc),1))
	#turn data into 2D array with time value as first column
	minhunk = np.c_[tc,np.transpose(mins)]
	maxhunk = np.c_[tc,np.transpose(maxes)]
	meanhunk = np.c_[tc,np.transpose(means)]
	#put together header string with names and units
	hdr = 't'
	for name in names:
		hdr = hdr + '\t'+name
	hdr = hdr + '\n'
	hdr = hdr + '[s]'
	for unit in units:
		hdr = hdr + '\t'+'['+unit+']'
	print 'Printing min/max/mean stats to '+dirName+'/'
	np.savetxt(dirName+'/'+fNamePrefix+"_mins.csv", minhunk, delimiter=",",header=hdr)
	np.savetxt(dirName+'/'+fNamePrefix+"_maxes.csv", maxhunk, delimiter=",",header=hdr)
	np.savetxt(dirName+'/'+fNamePrefix+"_means.csv", meanhunk, delimiter=",",header=hdr)

#def Chug(fnames,start_i,end_i,tList,var,names,vList,vMins,vMaxs,vMeans,CrdLoc,DtaLoc,DmnLoc):
def Chug(fnames,start_i,end_i,var,names,CrdLoc,DtaLoc,DmnLoc):

	#Initialize variable containers
	vList = InitVarList(var)
	vMins = InitVarList(var)
	vMeans = InitVarList(var)
	vMaxs = InitVarList(var)

	tList = []
	#x = []; y = []; z = [];
	iDump = -1
	chunkSize = np.subtract(start_i,end_i)
	SDim = np.count_nonzero(chunkSize)
	print 'Specified ',SDim,'dimensional space'
	for ifile in range(len(filenames)):
		filename = fnames[ifile]
		if filename.endswith(".h5") and filename.startswith("PlasCom2_"):
			iDump += 1
			print str(int(100.0*ifile/(len(filenames)-1)))+'% :'+' Opening ',filename
			f = h5py.File(filename, "r")
			t = GetSnapShotTime(f,DmnLoc)
			tList.append(t)
			print 'time: ', t
			#only need grid data from first file (assuming no changing meshes)
			try:
				ytmp
			except NameError:
				print "Grabbing grid data"
				
				xtmp,ytmp,ztmp,Xtmp,Ytmp,Ztmp = GetCoords(f,CrdLoc,start_i,end_i)
				print xtmp.shape, ytmp.shape, ztmp.shape
				xc = xtmp
				yc = ytmp
				zc = ztmp
				print 'X min/max',np.amin(xc),np.amax(xc)
				print 'Y min/max',np.amin(yc),np.amax(yc)
				print 'Z min/max',np.amin(zc),np.amax(zc)
			else:
				print "Re-using grid data from first dump"
			#pull the desired variable data from the file
			for i in range(len(var)):
				varInFile = DtaLoc+var[i] in f
				if varInFile:
					varSlice = GetDataSlice_ijk(f,DtaLoc+var[i],start_i,end_i)
					vList[i].append(varSlice)
					vMins[i].append(np.amin(vList[i][iDump]))
					vMeans[i].append(np.mean(vList[i][iDump]))
					vMaxs[i].append(np.amax(vList[i][iDump]))
					print names[i],' min / mean / max:',np.amin(vList[i][iDump]),np.mean(vList[i][iDump]),np.amax(vList[i][iDump])
				else:
					print 'ERROR: ',DtaLoc+var[i], 'NOT FOUND IN', filename, '!!!!!!!'
			f.close()
			print '\n'
	#convert times to numpy array (necessary for plots, etc)
	tList = np.asarray(tList)
	return tList,xc,yc,zc,Xtmp,Ytmp,Ztmp,var,names,vList,vMins,vMaxs,vMeans

#plot variables in 3d hunk of grid
def PlotAllStates_Cube(xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means):
	dirName = 'figures'
	if not os.path.exists(dirName):
		os.mkdir(directory+'/'+dirName)
		print "Directory " , directory+'/'+dirName ,  " Created "
	# Set up the axes with gridspec
	n_plt_grid=10
	plot_height=5
	tcfac = 1e6
	lenfac = 1000
	if xc.size > 1:
		xcoords = xc
		xcName = 'x (mm)'
	if yc.size > 1:
		ycoords = yc
		ycName = 'y (mm)'
	if zc.size > 1:
		zcoords = zc
		zcName = 'z (mm)'
	xmin = np.amin(xcoords);	xmax = np.amax(xcoords)
	ymin = np.amin(ycoords);	ymax = np.amax(ycoords)
	zmin = np.amin(zcoords);	zmax = np.amax(zcoords)
	
	#get the aspect ratio for the plane of interest (and scale plots accordingly)
	Aspect_ratio = (ymax-ymin)/(xmax-xmin)
	if Aspect_ratio > 1.0:		
		Main_width = int(max(round(plot_height * Aspect_ratio),1))
		Main_height = plot_height
	elif Aspect_ratio <= 1.0:		
		Main_width = plot_height
		Main_height = int(max(round(plot_height * Aspect_ratio),1))
	print 'visualizing data...'	
	for iStep in range(len(tc)):
		print 'Step Number ',iStep,'/',len(tc)-1
		for iVar in range(len(names)):
			print '\t', names[iVar]
			fig = plt.figure(figsize=(9, 9))
			grid = plt.GridSpec(n_plt_grid, n_plt_grid, hspace=0.15, wspace=0.15)
			x_plot = fig.add_subplot(grid[-plot_height:None, 0:plot_height])
			y_plot = fig.add_subplot(grid[:-plot_height, 0:-plot_height])
			z_plot = fig.add_subplot(grid[-plot_height:None, plot_height:])
			t_plot = fig.add_subplot(grid[0:-plot_height, plot_height:])
	
			y_plot.xaxis.tick_top()
			y_plot.xaxis.set_label_position('top') 
			z_plot.yaxis.tick_right()
			z_plot.yaxis.set_label_position('right') 
			t_plot.yaxis.tick_right()
			t_plot.yaxis.set_label_position('right') 
			t_plot.xaxis.tick_top()
			t_plot.xaxis.set_label_position('top') 
			x_plot.grid(True)
			y_plot.grid(True)
			z_plot.grid(True)
			t_plot.grid(True)
    
	    	# grab the data we're interested in
			Q = np.transpose(varLst[iVar][iStep])
			Qx,Qy,Qz = GetXYZMeans(Q)
			x_plot.set_xlim([lenfac*xmin, lenfac*xmax])
			y_plot.set_xlim([lenfac*ymin, lenfac*ymax])
			z_plot.set_xlim([lenfac*zmin, lenfac*zmax])
			x_plot.set_xlabel(xcName)
			y_plot.set_xlabel(ycName)
			z_plot.set_xlabel(zcName)
			z_plot.plot(lenfac*zcoords,scales[iVar]*Qz,'r', linewidth=3)
			z_plot.set_ylabel('xy-averaged '+names[iVar]+' ('+units[iVar]+')')
			y_plot.plot(lenfac*ycoords,scales[iVar]*Qy,'r', linewidth=3)
			y_plot.set_ylabel('xz-averaged '+names[iVar]+' ('+units[iVar]+')')
			x_plot.plot(lenfac*xcoords,scales[iVar]*Qx,'r', linewidth=3)
			x_plot.set_ylabel('yz-averaged '+names[iVar]+' ('+units[iVar]+')')
			z_plot.set_ylim((scales[iVar]*min(mins[iVar]),scales[iVar]*max(maxes[iVar])))
			y_plot.set_ylim((scales[iVar]*min(mins[iVar]),scales[iVar]*max(maxes[iVar])))
			x_plot.set_ylim((scales[iVar]*min(mins[iVar]),scales[iVar]*max(maxes[iVar])))
			t_plot.set_ylim((scales[iVar]*min(mins[iVar]),scales[iVar]*max(maxes[iVar])))
			t_plot.set_xlim((min(tc)*tcfac,max(tc)*tcfac))
			t_plot.set_ylabel('average '+names[iVar]+' ('+units[iVar]+')')
			t_plot.set_xlabel('t (us)')
			t_plot.plot(tc*tcfac,scales[iVar]*np.asarray(means[iVar]))
			t_plot.plot(tc[iStep]*tcfac,scales[iVar]*means[iVar][iStep], marker='o', markersize=4, color="red")
			figname=names[iVar]+'_'+str(iStep).zfill(3)
			fig.savefig(directory+'/'+dirName+'/'+figname+".png", bbox_inches='tight', dpi=300)
			plt.close()
	return 0
#plot variables along 2d slice of grid
def PlotAllStates_Slice(xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means):
	dirName = 'figures'
	if not os.path.exists(dirName):
		os.mkdir(directory+'/'+dirName)
		print "Directory " , directory+'/'+dirName ,  " Created "
	# Set up the axes with gridspec
	n_plt_grid=10
	plot_height=5
	tcfac = 1e6
	lenfac = 1000
	if xc.size > 1:
		xcoords = xc
		xcName = 'x (mm)'
	if yc.size > 1 and (xc.size > 1 or zc.size > 1):
		ycoords = yc
		ycName = 'y (mm)'
	elif yc.size > 1 and (xc.size <= 1 and zc.size <= 1):
		xcoords = yc
		xcName = 'y (mm)'
	if zc.size > 1 and yc.size > 1:
		xcoords = zc
		xcName = 'z (mm)'
	elif zc.size > 1 and xc.size > 1:
		ycoords = zc
		ycName = 'z (mm)'
	xmin = np.amin(xcoords);	xmax = np.amax(xcoords)
	ymin = np.amin(ycoords);	ymax = np.amax(ycoords)
	
	#get the aspect ratio for the plane of interest (and scale plots accordingly)
	Aspect_ratio = (ymax-ymin)/(xmax-xmin)
	if Aspect_ratio > 1.0:		
		Main_width = int(max(round(plot_height * Aspect_ratio),1))
		Main_height = plot_height
	elif Aspect_ratio <= 1.0:		
		Main_width = plot_height
		Main_height = int(max(round(plot_height * Aspect_ratio),1))
	print 'visualizing data...'	
	for iStep in range(len(tc)):
		print 'Step Number ',iStep,'/',len(tc)-1
		for iVar in range(len(names)):
			print '\t', names[iVar]
			fig = plt.figure(figsize=(10, 6))
			grid = plt.GridSpec(n_plt_grid, n_plt_grid, hspace=0.15, wspace=0.15)
			main_ax = fig.add_subplot(grid[-Main_height:None, 0:Main_width])
			y_plot = fig.add_subplot(grid[:-Main_height, 0:-plot_height], sharex=main_ax)
			z_plot = fig.add_subplot(grid[-Main_height:None, Main_width:], sharey=main_ax)
			t_plot = fig.add_subplot(grid[0:-(Main_height+2), -plot_height+1:])
			# make the plots look pretty (no clashing/overlapping labels)	
			y_plot.xaxis.tick_top()
			y_plot.xaxis.set_label_position('top') 
			z_plot.yaxis.tick_right()
			z_plot.yaxis.set_label_position('right') 
			t_plot.yaxis.tick_right()
			t_plot.yaxis.set_label_position('right') 
    
			z_plot.spines['top'].set_visible(False)
			z_plot.spines['right'].set_visible(False)
			z_plot.spines['bottom'].set_visible(False)
    
			y_plot.spines['top'].set_visible(False)
			y_plot.spines['right'].set_visible(False)
			y_plot.spines['left'].set_visible(False)

	    	# grab the data we're interested in
			Q = np.transpose(varLst[iVar][iStep])
			Qy,Qz = GetYZMeans(Q)
			if Qy.shape != xcoords.shape: # then we screwed something up
				Q = varLst[iVar][iStep]
				Qy,Qz = GetYZMeans(Q)

			main_ax.contourf(lenfac*xcoords,lenfac*ycoords,scales[iVar]*Q,50, cmap=cm.jet)
			main_ax.set_xlim([lenfac*xmin, lenfac*xmax])
			main_ax.set_ylim([lenfac*ymin, lenfac*ymax])
			main_ax.set_xlabel(xcName)
			main_ax.set_ylabel(ycName)
			z_plot.plot(scales[iVar]*Qz,lenfac*ycoords,'r', linewidth=3)
			z_plot.set_xlabel(names[iVar]+' ('+units[iVar]+')')
			z_plot.set_ylabel(ycName)
			main_ax.set_yticks([-5,-6.5,-8])
			z_plot.grid(True)
			y_plot.plot(lenfac*xcoords,scales[iVar]*Qy,'r', linewidth=3)
			y_plot.set_ylabel(names[iVar]+' ('+units[iVar]+')')
			y_plot.set_xlabel(xcName)
			y_plot.set_ylim((scales[iVar]*min(mins[iVar]),scales[iVar]*max(maxes[iVar])))
			z_plot.set_xlim((scales[iVar]*min(mins[iVar]),scales[iVar]*max(maxes[iVar])))
			t_plot.set_ylim((scales[iVar]*min(mins[iVar]),scales[iVar]*max(maxes[iVar])))
			t_plot.set_xlim((min(tc)*tcfac,max(tc)*tcfac))
			t_plot.set_ylabel('average '+names[iVar]+' ('+units[iVar]+')')
			t_plot.set_xlabel('t (us)')
			t_plot.grid(True)
			t_plot.plot(tc*tcfac,scales[iVar]*np.asarray(means[iVar]))
			t_plot.plot(tc[iStep]*tcfac,scales[iVar]*means[iVar][iStep], marker='o', markersize=4, color="red")
			y_plot.grid(True)
			figname=names[iVar]+'_'+str(iStep).zfill(3)
			fig.savefig(directory+'/'+dirName+'/'+figname+".png", bbox_inches='tight', dpi=300)
			plt.close()
	return 0
#plot variables along 1d line of points
def PlotAllStates_Line(xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means):
	dirName = 'figures'
	if not os.path.exists(dirName):
		os.mkdir(directory+'/'+dirName)
		print "Directory " , directory+'/'+dirName ,  " Created "
	# Set up the axes with gridspec
	n_plt_grid=10
	plot_height=5
	tcfac = 1e6
	lenfac = 1000
	if xc.size > 1:
		xcoords = xc
		xcName = 'x (mm)'
	elif yc.size > 1:
		xcoords = yc
		xcName = 'y (mm)'
	if zc.size > 1:
		xcoords = zc
		xcName = 'z (mm)'
	xmin = np.amin(xcoords);	xmax = np.amax(xcoords)
	
	print 'visualizing data...'	
	for iStep in range(len(tc)):
		print 'Step Number ',iStep,'/',len(tc)-1
		for iVar in range(len(names)):
			print '\t', names[iVar]
			fig = plt.figure(figsize=(10, 5))
			grid = plt.GridSpec(n_plt_grid, n_plt_grid, hspace=0.15, wspace=0.15)
			y_plot = fig.add_subplot(grid[:, 0:plot_height])
			t_plot = fig.add_subplot(grid[:, -plot_height:])
	
			t_plot.yaxis.tick_right()
			t_plot.yaxis.set_label_position('right') 
    
	    	# grab the data we're interested in
			Q = np.transpose(varLst[iVar][iStep])
			y_plot.plot(lenfac*xcoords,scales[iVar]*Q,'r', linewidth=3)
			y_plot.set_ylabel(names[iVar]+' ('+units[iVar]+')')
			y_plot.set_xlabel(xcName)
			y_plot.set_ylim((scales[iVar]*min(mins[iVar]),scales[iVar]*max(maxes[iVar])))
			t_plot.set_xlim((min(tc)*tcfac,max(tc)*tcfac))
			t_plot.set_ylabel('average '+names[iVar]+' ('+units[iVar]+')')
			t_plot.set_xlabel('t (us)')
			t_plot.grid(True)
			t_plot.plot(tc*tcfac,scales[iVar]*np.asarray(means[iVar]))
			t_plot.plot(tc[iStep]*tcfac,scales[iVar]*means[iVar][iStep], marker='o', markersize=4, color="red")
			y_plot.grid(True)
			figname=names[iVar]+'_'+str(iStep).zfill(3)
			fig.savefig(directory+'/'+dirName+'/'+figname+".png", bbox_inches='tight', dpi=300)
			plt.close()
	return 0
#plot variable values at point over time
def PlotAllStates_Point(xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means):
	dirName = 'figures'
	if not os.path.exists(dirName):
		os.mkdir(directory+'/'+dirName)
		print "Directory " , directory+'/'+dirName ,  " Created "
	# Set up the axes with gridspec
	n_plt_grid=10
	plot_height=5
	tcfac = 1e6
	lenfac = 1000
	
	print 'visualizing data...'	
	for iVar in range(len(names)):
		print '\t', names[iVar]
		fig = plt.figure(figsize=(5, 5))
		grid = plt.GridSpec(n_plt_grid, n_plt_grid, hspace=0.15, wspace=0.15)
		t_plot = fig.add_subplot(grid[:, :])
	   	# grab the data we're interested in
		t_plot.set_xlim((min(tc)*tcfac,max(tc)*tcfac))
		t_plot.set_ylabel(names[iVar]+' ('+units[iVar]+')')
		t_plot.set_title(names[iVar]+' at (x,y,z)='+str('%6.3f'%np.mean(xc))+','+str('%6.3f'%np.mean(yc))+','+str('%6.3f'%np.mean(zc)))
		t_plot.set_xlabel('t (us)')
		t_plot.grid(True)
		t_plot.plot(tc*tcfac,scales[iVar]*np.asarray(means[iVar]))
		figname=names[iVar]+'_tAvg'
		fig.savefig(directory+'/'+dirName+'/'+figname+".png", bbox_inches='tight', dpi=300)
		plt.close()
	return 0

#general viz handler
def MakePlots(ND,xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means):
	if ND == 0:
		PlotAllStates_Point(xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means)
	elif ND == 1:
		PlotAllStates_Line(xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means)
	elif ND == 2:
		PlotAllStates_Slice(xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means)
	elif ND == 3:
		PlotAllStates_Cube(xc,yc,zc,varLst,names,units,scales,tc,mins,maxes,means)
	else:
		print 'MakePlots: Specified space (',ND,') is not [0-3] dimensional.'
	return 0
#find all instances of something in a string
def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub)

def InjectNumpyFunc(UDFraw,opstr,newopstr):
		mloc = 1 
		while True:
			mloc = UDFraw.find(opstr)
			if mloc >=1:
				UDFraw = UDFraw[0:mloc-1]+','+UDFraw[mloc+1:]
				m1loc = UDFraw[0:mloc-1].rfind(' ')
				m2loc = UDFraw[mloc+2:].find(' ')
				if m2loc >= 0:
					UDFraw = UDFraw[0:m2loc+mloc+2]+')'+UDFraw[m2loc+mloc+3:]
				else: 
					UDFraw = UDFraw + ')'
				if m1loc >= 0:
					UDFraw = UDFraw[0:m1loc]+newopstr+UDFraw[m1loc+1:]
				else: 
					UDFraw = newopstr+UDFraw
			else:
				break
		return UDFraw

#interpret UDF's
def ParseUDF(UDFstr,names):
	UDFtmp = UDFstr
	for i in range(len(names)):
		newname = 'vList['+str(i)+'][iS]'
		UDFtmp = [s.replace(names[i] , newname) for s in UDFtmp]
#replace problematic operators
	for i in range(len(UDFtmp)):
#		UDF = InjectNumpyFunc(UDF,'*','np.multiply(') #numpy overloading actually allows these
#		UDF = InjectNumpyFunc(UDF,'/','np.divide(')	#... and these
		UDFtmp[i] = UDFtmp[i].replace('sqrt' , 'np.sqrt')
	return UDFtmp
#Calculate UDF's
def ProcessUDFs(vstrs,vList,UDFstrs,UDFnms,ts):

	if len(UDFstrs) > 0:
		Ulst = InitVarList(UDFstrs)
		UMaxs = InitVarList(UDFstrs)
		UMins = InitVarList(UDFstrs)
		UMeans = InitVarList(UDFstrs)
		UDFstrs = ParseUDF(UDFstrs,vstrs)
		print 'Detected '+str(len(UDFstrs))+' User Defined Functions.'
		for i in range(len(UDFstrs)):
			print '\t Parsed '+UDFnms[i]+': '+UDFstrs[i]

	if len(Ulst) > 0:
		print 'Computing '+str(len(Ulst))+' UDFs:'
		for iS in range(np.shape(vList)[1]):	
			print 'time: ',ts[iS]
			for i in range(len(Ulst)):
				Utmp = eval(UDFstrs[i])	
				Ulst[i].append(Utmp)
				UMaxs[i].append(np.amax(Utmp))
				UMins[i].append(np.amin(Utmp))
				UMeans[i].append(np.mean(Utmp))
				print '\t'+UDFnms[i]+' min / mean / max:', np.amin(Utmp),np.mean(Utmp),np.amax(Utmp)
	else:
		Ulst=[]
		UMaxs=[]
		UMins=[]
		UMeans=[]
	return Ulst,UMaxs,UMins,UMeans
################## END OF PREDEFINED FUNCTIONS ###############################	
		
	
# user inputs (desired variables, names, units, scales (dimensionalization))    
variables = ['rho','velocity-1','velocity-2','velocity-3','scalarVars-2','pressure','temperature']
varNames = ['rho','vx','vy','vz','O2','pressure','temperature']
varUnits = ['kg/m^3','m/s','m/s','m/s','kg/m^3','kPa','K']
varScales = [1.0,1.0,1.0,1.0,1.0,1.0e-3,1.0]
#UDFS are defined in terms of variables, not varNames
UDFs = ['0.5 * rho * sqrt( velocity-1 * velocity-1 + velocity-2 * velocity-2 + velocity-3 * velocity-3 )', 'scalarVars-2 / rho']
UDFNames = ['KE', 'YO2']
UDFUnits = ['J/m^3','unitless']
UDFScales = [1.0,1.0]
#grid indices you're interested in
start_ind = [-1,0,0]
end_ind = [-1,-1,-1]
#Input grid/simulation data and file locations
BaseName='PlasCom2'
DomainName='PlasComCM'
GeomName='cmgeom'
GridNum='23'
directory = '.'

#The data locations below should not change
CoordLoc='/'+BaseName+'/Geometry/'+GeomName+'/Group0'+GridNum+'/'
DataLoc='/'+BaseName+'/Simulation/'+DomainName+'/grid'+GridNum+'/'
DomainLoc='/'+BaseName+'/Simulation/'+DomainName+'/'

#Initialize variable containers
nDims = np.count_nonzero(np.subtract(start_ind,end_ind))
filenames = sorted(os.listdir(directory))

#chug through the specified files and get data from the plascom2 h5 dumps  
times,x,y,z,X,Y,Z,variables,varNames,varList,varMins,varMaxs,varMeans = \
Chug(filenames,start_ind,end_ind,variables,varNames,CoordLoc,DataLoc,DomainLoc)
#calculate UDFs from the collected data
UDFList,UDFMaxs,UDFMins,UDFMeans = ProcessUDFs(variables,varList,UDFs,UDFNames,times)
#################################
#Put Analysis Functions down here
#################################

###print min/max/mean of variables in region to csv files
PrintStatsToCSV('Q',varNames,varUnits,times,varMins,varMaxs,varMeans)
###print min/max/mean of UDFs in region to csv files
if np.size(UDFs) > 0:
	PrintStatsToCSV('UDF',UDFNames,UDFUnits,times,UDFMins,UDFMaxs,UDFMeans)
###go through all collected data and visualize
MakePlots(nDims,x,y,z,varList,varNames,varUnits,varScales,times,varMins,varMaxs,varMeans)
###go through all UDFs and visualize
if np.size(UDFs) > 0:
	MakePlots(nDims,x,y,z,UDFList,UDFNames,UDFUnits,UDFScales,times,UDFMins,UDFMaxs,UDFMeans)
SaveDatatoFile('DC_VarState',times,variables,varNames,udfvals=UDFList,udfnames=UDFNames,xc=X,yc=Y,zc=Z)

print 'Done'
